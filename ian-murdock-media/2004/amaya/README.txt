Photos and videos from Amaya M. Rodrigo Sastre

Attribution-NonCommercial-ShareAlike
https://creativecommons.org/licenses/by-nc-sa/2.0/

The photos are mainly from 3 events:

* Debconf 4. May 26th to June 2nd at SESC Porto Alegre, Brazil.
http://debconf4.debconf.org/

* 5th Fórum Internacional do Software Livre, FISL, June 3rd to 5th at PUCRS Convention Center in Porto Alegre, Brazil.

* III Jornades de Programari Lliure July 7-10 at Escola de Manresa, Universitat Politècnica de Catalunya, Catalunya, Spain
http://jpl.cpl.upc.edu/iii-jornades

Also from Amaya: https://gallery.debconf.org/main.php?g2_itemId=13054

