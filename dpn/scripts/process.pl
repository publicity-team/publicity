#!/usr/bin/perl

# Author:       2011-2012 David Prévot <taffit@debian.org>
# License:      GPLv2 or later or, at your option, MIT (EXPAT)

# Usage: run it after get-new.sh to convert the “list” file in the expected format

open LIST, '<list' or die;
while (<LIST>){
	chomp;
	my $description = `apt-cache show $_ | grep Description-en: | sed -e 's/Description-en: //'`;
	chomp $description;
	print '<li><a href="https://packages.debian.org/unstable/main/'.$_.'">'.$_." — $description</a></li>\n";
}
close LIST;
