#!/usr/bin/python3
from pypump import PyPump, Client
from datetime import datetime
from pprint import pprint
from os import mkdir
from os import utime


def verifier(url):
    print('Verify URL: %s' % url)
    return raw_input('Verifier: ')


client = Client(
    webfinger='debian@identi.ca',
    name='Debian identi.ca backup',
    type='native'
)

template_file = '%s/%s.wml'
template_title = '#use wml::debian::template title="%s"\n'
template_text = '''%(title)s#use wml::debian::news::short
%(text)s
<posted timestamp="%(modified)s" url="%(identica)s"/>
'''

pump = PyPump(client=client, verifier_callback=verifier)

outbox = pump.me.outbox

try:
    mkdir('errors')
except OSError:
    pass

i = 0
for activity in outbox:
    note = activity.obj
    if note.object_type == 'comment':
        error = 'comment'
    elif note.object_type != 'note':
        error = 'not-a-note'
    elif note.deleted:
        error = 'deleted'
    elif note.in_reply_to:
        error = 'comment'
    elif not note.content:
        error = 'no-content'
    elif not note.published:
        error = 'no-published'
    elif not note.updated:
        error = 'no-updated'
    elif not note.url:
        error = 'no-url'
    else:
        error = None
    if error:
        with open('errors/%s-%s' % (error, i), 'w', encoding='UTF-8') as error_f:
            pprint(activity.__dict__, stream=error_f)
            pprint(note.__dict__, stream=error_f)
        i += 1
        continue
    title = note.display_name
    text = note.content
    created = note.published or activity.published
    modified = note.updated or note.published or activity.updated or activity.published
    url = note.url
    if title:
        title = template_title % title
    else:
        title = ''
    year = str(created.year)
    modified_sec = modified.timestamp()
    created_sec = created.timestamp()
    created = str(created)
    created = created.replace('+00:00', '')
    note_name = template_file % (year, created)
    note_name = note_name.translate(str.maketrans('', '', '- :'))
    try:
        mkdir(year)
    except OSError:
        pass
    with open(note_name, 'w', encoding='UTF-8') as note_f:
        print(template_text % {
            'title': title,
            'text': text,
            'created': created_sec,
            'modified': modified_sec,
            'identica': url,
            },
            file=note_f)
    utime(note_name, (modified_sec, modified_sec))
