This file is to prepare/update Debian's description for the SPI website

Status: [needs-review]

# Current link of the page

https://http://spi-inc.org/projects/debian/

# Current text

2019-10-16

Debian GNU/Linux is a free operating system (OS) for your computer. An operating system is the set of basic programs and utilities that make your computer run. Debian uses the Linux kernel (the core of an operating system), but most of the basic OS tools come from the GNU project; hence the name GNU/Linux.

The Debian operating system is POSIX based, freely distributable, and includes features such as true multitasking, virtual memory, shared libraries, demand loading, proper memory management, TCP/IP networking, and other features consistent with Unix-type systems. It is used by individuals and organizations worldwide.

# Proposal

The Debian Project was founded in 1993 by Ian Murdock to be a truly free community project. Since then the project has grown to be one of the largest and most influential open source projects. Thousands of volunteers from all over the world work together to create and maintain Debian software. Available in 70 languages, and supporting a huge range of computer types, Debian calls itself the "universal operating system". 

# Notes

2019-10-16: Proposal taken from the "About Debian" section in DebConf closing announcement.
