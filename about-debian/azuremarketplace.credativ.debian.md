This file is to prepare/update Debian's description for the Microsoft Azure Marketplace

Status: [needs-review]

# Current link of the page

https://azuremarketplace.microsoft.com/en-us/marketplace/apps/credativ.debian

# Current link to documentation about the page

https://docs.microsoft.com/en-us/azure/marketplace/cloud-partner-portal/azure-applications/cpp-marketplace-tab

# Icon to be used (URL in Debian infrastructure)

# Title
Debian

# Subtitle
Debian

## Categories
Compute

## Legal
[License Agreement](https://www.debian.org/social_contract)
[Privacy Policy](https://www.debian.org/social_contract)

## Stars

2019-07-11: 0


## Reviews

2019-07-11: 0


## Overview tab

### Title

Debian: The universal operating system

### Subtitle

Debian 8 "Jessie" and 9 "Stretch" for Microsoft Azure.

### Learn more

[General documentation](https://www.debian.org/doc/)
[Debian on Azure documentation](https://wiki.debian.org/Cloud/MicrosoftAzure)


## Plans tab

Plan	Description
Debian 8 "Jessie"	Debian 8 "Jessie" for Microsoft Azure.
Debian 8 "Jessie" with backports kernel	Debian 8 "Jessie" with backports kernel for Microsoft Azure.
Debian 9 "Stretch"	Debian 9 "Stretch" for Microsoft Azure.
Debian 9 "Stretch" with backports kernel	Debian 9 "Stretch" with backports kernel.

## Reviews tab

2019-07-11 : No reviews are available.
