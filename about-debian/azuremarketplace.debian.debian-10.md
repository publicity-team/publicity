This file is to prepare/update Debian's description for the Microsoft Azure Marketplace

Status: [needs-review]

# Current link of the page

https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-10

# Current link to documentation about the page

https://docs.microsoft.com/en-us/azure/marketplace/cloud-partner-portal/azure-applications/cpp-marketplace-tab

# Icon to be used (URL in Debian infrastructure)

https://salsa.debian.org/cloud-team/logos/tree/master/Azure

# Common stuff

## Marketplace Title

```
Debian 10 "Buster"
```

## Publisher

```
Debian
```

## Categories
Compute

## Legal

* [License Agreement](https://www.debian.org/social_contract)
* [Privacy Policy](https://www.debian.org/legal/privacy)

(Have to be configured per Sku, unclear which one if taken for the main marketplace view.)

## Stars

2019-07-11: 0


## Reviews

2019-07-11: 0


# Overview tab

## Marketplace Summary

```
Debian: The universal operating system
```

## Marketplace Description

```
Debian 10 "Buster" for Microsoft Azure.
```

(Can contain simple HTML.)

## Learn more

* [General documentation](https://www.debian.org/doc/)
* [Debian on Azure documentation](https://wiki.debian.org/Cloud/MicrosoftAzure)

(Have to be configured per Sku/Cloud, unclear which one if taken for the main marketplace view.)

# Plans tab

## Sku Title

```
Debian 10 "Buster"
```

```
Debian 10 "Buster" with backports kernel
```

## Sku Description

```
Debian 10 "Buster" for Microsoft Azure.
```

(Have to be configured per Cloud, unclear which one if taken for the main marketplace view.)

# Reviews tab

2019-07-11 : No reviews are available.
