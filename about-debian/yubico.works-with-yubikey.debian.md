This file is to prepare/update Debian's entry in the "Works with Yubikey" Catalog

Status: [submitted-2018-10-18 via partners@debian.org]

# Current link of the page

https://www.yubico.com/works-with-yubikey/catalog/debian/

# Current link to documentation about the page

Form from yubico
https://docs.google.com/forms/d/e/1FAIpQLSeCvzn_ldqVDdQDU_0VBuJ_nx0hWwG_6wSWUDHV8UgHOgiKkQ/viewform

# Works with YubiKey - Catalog Entry

Congratulations! We're happy to welcome you into the Yubico Technology Partner Program. Let's start with getting you listed in our Works with YubiKey catalog (yubico.com/solutions). Please complete the form below and provide the required information for the listing. 
Note: You should submit this form for each of your products which support Yubico hardware.

We also require your logo for the listing. Please email your high-res company logo (.png/.jpeg/.svg/.psd/.ai file) and, if available, product screen shots/imagery (.png/.jpeg/.svg/.psd/.ai file) to your Yubico Marketing representative.

# Page 1 of the form

## Email * 
partners@debian.org 

## Company Name as it should appear on our site *

Debian

## Primary Listing Category (Select all that apply.) *
[*] Computer Login
* Password Management
* Identity and Access Management
* Authentication
[*] Developer Platform
* Other:

## Secondary Listing Category (Select all that apply.) *  [NEEDS-REVIEW]

(I would check all of them...)

* Encryption
* Privileged Access
* Virtual Private Network (VPN)
* Content Management System (CMS)
* Remote Access
* Computer Login
* Identity and Access Management
* Developer Tools
* Password Management
* Cloud Service
* Developer Tools
* System Integrators
* Other: 

## Where can customers purchase your product on your site? (Perm link) *

https://www.debian.org

## Where can customers find YubiKey setup instructions on your site? (Perm link) *  [NEEDS-REVIEW]

https://wiki.debian.org/Smartcards/YubiKey4 (??)

Currently our entry links to: http://uname.pingveno.net/blog/index.php/post/2013/08/06/Configure-2-factor-Yubikey-authentication-for-Debian-%3A-the-easiest-way

## Where can customers can contact your support team or knowledge base on your site? (Perm link) * 

https://www.debian.org/support

## Where can customers find your product's technical specs on your site? (Perm link) 

https://www.debian.org/releases/

# Page 2 of the form

## Partner Contacts  [NEEDS-REVIEW]
Let us know who to contact for coordination and collaboration with your team.
* Marketing Contact Email * partners@debian.org
* Marketing Contact Name * Debian Partners Team 
* Marketing Contact Phone Number *         None
* Technical Contact Email * pkg-auth-maintainers@alioth-lists.debian.net (??)
* Technical Contact Name * pkg-auth maintainers list
* Technical Contact Phone Number * none

# Product Information

Share general product information with us for marketing materials.
## Product Name as it should appear on our site *

Debian

## Product Version as it should appear on our site * 

Debian 9 Stretch

## Product Tagline (130 characters or less) *

The Universal Operating System

## Product description * 

The Debian Project is an association of Free Software developers who volunteer their time and effort in order to produce the completely free operating system Debian.
Debian is known for its adherence to the Unix and Free Software philosophies, and for its extensiveness — the current release includes over 51,000 software packages for more than ten computer architectures, ranging from the more common Intel 32-bit and 64-bit architectures to ARM (cell phones and tablets) and the IBMS/390 (mainframes). Besides the well-known Linux kernel, it also supports the FreeBSD kernel.

## Problem Statement / Describe issue your product addresses (50 words or less) * 

The Debian Project was founded in 1993 by Ian Murdock as a new distribution which would be made openly, in the spirit of Linux and GNU. Debian was meant to be carefully and conscientiously put together, and to be maintained and supported with similar care. 

## Solution Statement / How your product provides solution (50 words or less) *  

Debian is a free operating system for your computer and for plenty of other systems which enhance our life. From the inner workings of your nearby airport to your car entertainment system, cloud servers hosting websites or the IoT devices that communicate with them, Debian can power it all.


## User Benefit Statement #1 (20 words or less) * 
The Debian project is a large and thriving organization with countless self-organized teams comprised of volunteers. 

## User Benefit Statement #2 (20 words or less) * 
The Debian Social Contract provides a vision of improving society

## User Benefit Statement #3 (20 words or less) * 
The Debian Free Software Guidelines provide an indication of what software is considered usable. 

## Certifications

https://www.debian.org/misc/awards

## Supported OS platforms *
Microsoft Windows
Windows Mobile
macOS
Classic Mac OS
iOS
[X] Linux
Android
Firefox OS
Other:

## Supported YubiKey authentication protocols * [NEEDS-REVIEW]
If YubiHSM, select "Other" and list which functions your product leverages.
[*] Yubico One Time Password (OTP)
[*] Universal 2nd Factor (U2F)
* FIDO2
[*] PIV-compatible Smart Card
[*] OpenPGP
[*] OATH
* Other:

## Supported web browsers * 
[*] Google Chrome
[*] Mozilla Firefox
* Safari
* Opera
* Internet Explorer
* Microsoft Edge
[*] Other: Many other free-software based browsers

 
